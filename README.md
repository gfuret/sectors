# README #

Homework assignment

### Install repository ###

Note for windows users:
This repository can be easily load in laragon for windows users:
Download repository to your www folder on laragon

Site open after following the steps bellow at:
http://sector.test/

Steps to install the app:

Run composer install/update

Create "sector" db on mysql server
Configure .env file "DATABASE_URL" for the specific configuration

Create tables with:
Run: php bin/console doctrine:migrations:migrate

Create some fake data with:
Run: php bin/console doctrine:fixtures:load


### About ###

Simple form with registration app

Not registered users:

App main page is dashboard, this page lists all published sector records from all users.

Registering as a user:

Can edit and add his own information


### Explanations ###

Aside from the common symfony libraries, some other ones were added like:

Fixture: is fundamental to add and delete data in order to fully assess a proper QA
Faker: creates fake data and works perfectly with fixture
Security-bundle: access control and auth (it was also required)
web-profiler: debug symfony tool
