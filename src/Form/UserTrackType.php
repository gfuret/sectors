<?php



namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use Symfony\Component\OptionsResolver\Options;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Repository\UserRepository;

class UserTrackType extends AbstractType
{
    private $loggedUser;

    public function __construct(UserRepository $userRepository)
    {
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add(
            'user',
            null,
            [
                'required' => true,
                'label' => 'Name of the user',
                'data' => $options['user'],
                'attr' => [
                    'disabled' => 'disabled'
                ]
            ]
        )
        ->add('sector', HiddenType::class, [])
        ->add('terms', CheckboxType::class, [
            'label'    => 'Agree to terms',
            'data' => $options['terms'],
            'required' => true,
        ])
        ->add('register', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success float-right'
            ]
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'user' => null,
            'terms' => null,
        ));
    }
}
