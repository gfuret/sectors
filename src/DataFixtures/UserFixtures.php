<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use App\Factory\UserFactory;

class UserFixtures extends Fixture
{
    public const POST_USER_REFERENCE = 'post-user';

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $userName = 'gfuret';
        $name = 'Gabi';
        $password = '123456';

        $user = new User();
        $user->setUsername($userName);
        $user->setName($name);
        $user->setPassword($this->encoder->encodePassword($user, $password));
        $user->setRoles([User::ROLE_USER]);

        $manager->persist($user);
        $manager->flush();

        UserFactory::new()->createMany(15);

        /**/

        //$this->addReference(self::POST_USER_REFERENCE, $user);
    }
}
