<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

use App\Entity\Sector;

class SectorFixtures extends Fixture
{
    private $sectors = [

        ['external_id'=>1, 'name'=>'Manufacturing', 'parent'=>null, 'level'=>1],
        ['external_id'=>19, 'name'=>'Construction materials', 'parent'=>1, 'level'=>2],
        ['external_id'=>18, 'name'=>'Electronics and Optics', 'parent'=>1, 'level'=>2],

        ['external_id'=>6, 'name'=>'Food and Beverage', 'parent'=>1, 'level'=>2],
        ['external_id'=>342, 'name'=>'Bakery & confectionery products', 'parent'=>6, 'level'=>3],
        ['external_id'=>43, 'name'=>'Beverages', 'parent'=>6, 'level'=>3],
        ['external_id'=>42, 'name'=>'Fish & fish products', 'parent'=>6, 'level'=>3],
        ['external_id'=>40, 'name'=>'Meat & meat products', 'parent'=>6, 'level'=>3],
        ['external_id'=>39, 'name'=>'Milk & dairy products', 'parent'=>6, 'level'=>3],
        ['external_id'=>437, 'name'=>'Other', 'parent'=>6, 'level'=>3],
        ['external_id'=>378, 'name'=>'Sweets & snack food', 'parent'=>6, 'level'=>3],

        ['external_id'=>13, 'name'=>'Furniture', 'parent'=>1, 'level'=>2],
        ['external_id'=>389, 'name'=>'Bathroom/sauna', 'parent'=>13, 'level'=>3],
        ['external_id'=>385, 'name'=>'Bedroom', 'parent'=>13, 'level'=>3],
        ['external_id'=>390, 'name'=>'Childrenâ€™s room', 'parent'=>13, 'level'=>3],
        ['external_id'=>98, 'name'=>'Kitchen', 'parent'=>13, 'level'=>3],
        ['external_id'=>101, 'name'=>'Living room ', 'parent'=>13, 'level'=>3],
        ['external_id'=>392, 'name'=>'Office', 'parent'=>13, 'level'=>3],
        ['external_id'=>394, 'name'=>'Other (Furniture)', 'parent'=>13, 'level'=>3],
        ['external_id'=>341, 'name'=>'Outdoor', 'parent'=>13, 'level'=>3],
        ['external_id'=>99, 'name'=>'Project furniture', 'parent'=>13, 'level'=>3],
 

        ['external_id'=>99, 'name'=>'Project furniture', 'parent'=>1, 'level'=>2],
        ['external_id'=>12, 'name'=>'Machinery', 'parent'=>99, 'level'=>3],
        ['external_id'=>94, 'name'=>'Machinery components', 'parent'=>99, 'level'=>3],
        ['external_id'=>91, 'name'=>'Machinery equipment/tools', 'parent'=>99, 'level'=>3],
        ['external_id'=>224, 'name'=>'Manufacture of machinery', 'parent'=>99, 'level'=>3],
        ['external_id'=>97, 'name'=>'Maritime', 'parent'=>99, 'level'=>3],
        ['external_id'=>271, 'name'=>'Aluminium and steel workboats', 'parent'=>97, 'level'=>4],
        ['external_id'=>269, 'name'=>'Boat/Yacht building', 'parent'=>97, 'level'=>4],
        ['external_id'=>230, 'name'=>'Ship repair and conversion', 'parent'=>97, 'level'=>4],
        ['external_id'=>93, 'name'=>'Metal structures', 'parent'=>99, 'level'=>3],
        ['external_id'=>508, 'name'=>'Other', 'parent'=>99, 'level'=>3],
        ['external_id'=>227, 'name'=>'Repair and maintenance service', 'parent'=>99, 'level'=>3],


        ['external_id'=>11, 'name'=>'Metalworking', 'parent'=>1, 'level'=>2],
        ['external_id'=>67, 'name'=>'Construction of metal structures', 'parent'=>11, 'level'=>3],
        ['external_id'=>263, 'name'=>'Houses and buildings', 'parent'=>11, 'level'=>3],
        ['external_id'=>267, 'name'=>'Metal products', 'parent'=>11, 'level'=>3],
        ['external_id'=>542, 'name'=>'Metal works', 'parent'=>11, 'level'=>3],
        ['external_id'=>75, 'name'=>'CNC-machining', 'parent'=>542, 'level'=>4],
        ['external_id'=>62, 'name'=>'Forgings, Fasteners', 'parent'=>542, 'level'=>4],
        ['external_id'=>69, 'name'=>'Gas, Plasma, Laser cutting', 'parent'=>542, 'level'=>4],
        ['external_id'=>66, 'name'=>'MIG, TIG, Aluminum welding', 'parent'=>542, 'level'=>4],

        ['external_id'=>9, 'name'=>'Plastic and Rubber', 'parent'=>1, 'level'=>2],
        ['external_id'=>54, 'name'=>'Packaging', 'parent'=>9, 'level'=>3],
        ['external_id'=>556, 'name'=>'Plastic goods', 'parent'=>9, 'level'=>3],
        ['external_id'=>559, 'name'=>'Plastic processing technology', 'parent'=>9, 'level'=>3],
        ['external_id'=>55, 'name'=>'Blowing', 'parent'=>559, 'level'=>4],
        ['external_id'=>57, 'name'=>'Moulding', 'parent'=>559, 'level'=>4],
        ['external_id'=>53, 'name'=>'Plastics welding and processing', 'parent'=>559, 'level'=>4],
        ['external_id'=>560, 'name'=>'Plastic profiles', 'parent'=>9, 'level'=>3],


        ['external_id'=>5, 'name'=>'Printing', 'parent'=>1, 'level'=>2],
        ['external_id'=>148, 'name'=>'Advertising', 'parent'=>5, 'level'=>2],
        ['external_id'=>150, 'name'=>'Book/Periodicals printing', 'parent'=>5, 'level'=>2],
        ['external_id'=>145, 'name'=>'Labelling and packaging printing', 'parent'=>5, 'level'=>2],

        ['external_id'=>7, 'name'=>'Textile and Clothing', 'parent'=>1, 'level'=>2],
        ['external_id'=>44, 'name'=>'Clothing', 'parent'=>7, 'level'=>3],
        ['external_id'=>45, 'name'=>'Textile', 'parent'=>7, 'level'=>3],

        ['external_id'=>8, 'name'=>'Wood', 'parent'=>1, 'level'=>2],
        ['external_id'=>337, 'name'=>'Other (Wood)', 'parent'=>8, 'level'=>2],
        ['external_id'=>51, 'name'=>'Wooden building materials', 'parent'=>8, 'level'=>2],
        ['external_id'=>47, 'name'=>'Wooden houses', 'parent'=>8, 'level'=>2],

        ['external_id'=>3, 'name'=>'Creative industries', 'parent'=>null, 'level'=>1],
        ['external_id'=>37, 'name'=>'Creative industries', 'parent'=>3, 'level'=>2],
        ['external_id'=>29, 'name'=>'Energy technology', 'parent'=>3, 'level'=>2],
        ['external_id'=>33, 'name'=>'Environment', 'parent'=>3, 'level'=>2],

        ['external_id'=>2, 'name'=>'Service', 'parent'=>null, 'level'=>1],
        ['external_id'=>25, 'name'=>'Business services', 'parent'=>2, 'level'=>2],
        ['external_id'=>35, 'name'=>'Engineering', 'parent'=>2, 'level'=>2],
        ['external_id'=>28, 'name'=>'Information Technology and Telecommunications', 'parent'=>2, 'level'=>2],
        ['external_id'=>581, 'name'=>'Data processing, Web portals, E-marketing', 'parent'=>28, 'level'=>3],
        ['external_id'=>576, 'name'=>'Programming, Consultancy', 'parent'=>28, 'level'=>3],
        ['external_id'=>121, 'name'=>'Software', 'parent'=>28, 'level'=>3],
        ['external_id'=>122, 'name'=>'Telecommunications', 'parent'=>28, 'level'=>3],
        ['external_id'=>22, 'name'=>'Tourism', 'parent'=>2, 'level'=>2],
        ['external_id'=>141, 'name'=>'Translation services', 'parent'=>2, 'level'=>2],
        ['external_id'=>21, 'name'=>'Transport and Logistics', 'parent'=>2, 'level'=>2],
        ['external_id'=>111, 'name'=>'Air', 'parent'=>21, 'level'=>3],
        ['external_id'=>114, 'name'=>'Rail', 'parent'=>21, 'level'=>3],
        ['external_id'=>112, 'name'=>'Road', 'parent'=>21, 'level'=>3],
        ['external_id'=>113, 'name'=>'Water', 'parent'=>21, 'level'=>3],

    ];

    public function load(ObjectManager $manager)
    {
        foreach ($this->sectors as $sector) {
            $manager->persist($this->insertSector($sector));
        }
        $manager->flush();
    }

    private function insertSector($sectorData)
    {
        $sector = new Sector();
        $sector->setExternalId($sectorData['external_id']);
        $sector->setName($sectorData['name']);
        $sector->setParentSector($sectorData['parent']);
        $sector->setLevel($sectorData['level']);
        return $sector;
    }
}
