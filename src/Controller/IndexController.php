<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\UserTrackRepository;

class IndexController extends AbstractController
{
    /**
     * @Route("/index", name="index")
     */
    public function index(UserTrackRepository $userTrack)
    {
        return $this->render('index/index.html.twig', [
            'tracks' => $userTrack->findAll(),
        ]);
    }
}
