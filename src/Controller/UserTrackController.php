<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

use App\Entity\User;
use App\Entity\Sector;
use App\Entity\UserTrack;

use App\Repository\SectorRepository;
use App\Repository\UserRepository;
use App\Repository\UserTrackRepository;

use Symfony\Component\Form\FormError;

use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\HttpFoundation\Request;

use App\Model\Sector as SectorModel;

use App\Form\UserTrackType;

class UserTrackController extends AbstractController
{
 

    /**
     * @Route("/user-track/{user}", name="sector_show")
     */
    public function sector(
        SectorRepository $sectorRepository,
        UserTrackRepository $userTrackRepository,
        Request $request,
        UserInterface $loggedUser
    ) {
        $userTrack = $userTrackRepository->findOneBy(['user'=>$loggedUser->getId()]);

        $selectedSector = null;
        if ($userTrack) {
            $selectedSector = $userTrack->getSector()->getExternalId();
        }

        $form = $this->createForm(
            UserTrackType::class,
            null,
            [
                'user'=> $loggedUser->getName(),
                'terms'=> empty($userTrack) ? false : $userTrack->getAgreeToTerms(),
            ]
        );
        
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            $sector = $sectorRepository->findOneBy(['externalId'=> $formData['sector']  ]);

            if (empty($sector)) {
                $form->get('sector')->addError(new FormError('Cannot find this sector, please refresh the page'));
            } elseif (empty($formData['terms'])) {
                $form->get('terms')->addError(new FormError('You must agree to terms to continue...'));
            } else {
                $em = $this->getDoctrine()->getManager();

                if (empty($userTrack)) {
                    $newUserTrack = new UserTrack();
                    $newUserTrack->setUser($loggedUser);
                    $newUserTrack->setSector($sector);
                    $newUserTrack->setAgreeToTerms((boolval($formData['terms'])));
                    $newUserTrack->setCreatedAt(new \DateTime());
                    $em->persist($newUserTrack);
                } else {
                    $userTrack->setSector($sector);
                    $userTrack->setAgreeToTerms((boolval($formData['terms'])));
                }

                $selectedSector = empty($newUserTrack) ? $userTrack->getSector()->getExternalId() : $newUserTrack->getSector()->getExternalId();

                $em->flush();

                $this->addFlash('success', 'Registration done! everything is doing great');
                
                $form = $this->createForm(UserTrackType::class, null, [
                    'user'=>$loggedUser->getName(),
                    'terms'=> empty($newUserTrack) ? $userTrack->getAgreeToTerms() : $newUserTrack->getAgreeToTerms()
                ]);
            }
        }

        return $this->render('user_track/new.html.twig', [
            'sectors'   => (new SectorModel($sectorRepository))->getOrganizedSectors(),
            'selectedSector'    => $selectedSector,
            'form'      => $form->createView(),
        ]);
    }
}
