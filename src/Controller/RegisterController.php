<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

use Symfony\Component\Form\FormError;

use App\Entity\User;

use Symfony\Component\HttpFoundation\Request;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegisterController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passEncoder)
    {
        $form = $this->createFormBuilder()
        ->add(
            'username',
            null,
            ['required'   => true]
        )
        ->add(
            'name',
            null,
            ['required'   => true]
        )
        ->add('password', RepeatedType::class, [
            'type' => PasswordType::class,
            'required' => true,
            'invalid_message' => 'The password fields must match.',
            'first_options'=>['label'=>'Password'],
            'second_options'=>['label'=>'Repeat again']
        ])
        ->add('register', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-success float-right'
            ]
        ])
        ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            if (empty($formData['username'])) {
                $form->get('username')->addError(new FormError('Username is required to create an account'));
            } elseif (empty($formData['name'])) {
                $form->get('name')->addError(new FormError('Name is required to create an account'));
            } else {
                $user = new User();
                $user->setUsername($formData['username']);
                $user->setName($formData['name']);
                $user->setPassword(
                    $passEncoder->encodePassword($user, $formData['password'])
                );
                $user->setRoles([User::ROLE_USER]);

                $em = $this->getDoctrine()->getManager();
                $em->persist($user);
                $em->flush();

                return $this->redirect($this->generateUrl('app_login'));
            }
        }


        return $this->render('register/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
