<?php

namespace App\Model;

use App\Repository\SectorRepository;

/**
 *
 */
class Sector
{
    private $sectorRepository;

    public function __construct(SectorRepository $sectorRepository)
    {
        $this->sectorRepository = $sectorRepository;
    }

    public function getOrganizedSectors()
    {
        $sectorTree = $this->buildTree($this->sectorRepository->findAll(array(), array('level' => 'ASC')));

        return $this->orderedSector($sectorTree);
    }

    private function buildTree(array $sectors, $parentId = 0)
    {
        $branch = [];

        foreach ($sectors as $sector) {
            if ($sector->getParentSector() == $parentId) {
                $children = $this->buildTree($sectors, $sector->getExternalId());
                if ($children) {
                    $sector->children = $children;
                }
                $branch[$sector->getExternalId()] = $sector;
            }
        }
        return $branch;
    }

    private function orderedSector($sectorTree, &$orderedSectors = [])
    {
        foreach ($sectorTree as $sector) {
            $orderedSectors[$sector->getExternalId()] = $sector;

            if (! empty($sector->children)) {
                $this->orderedSector($sector->children, $orderedSectors);
            }
        }
        return $orderedSectors;
    }
}
